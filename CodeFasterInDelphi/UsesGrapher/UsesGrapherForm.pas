unit UsesGrapherForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, System.Generics.Collections, DelphiAST.Classes;

type
  TProjectUnit = record
    UnitName : string;
    FileName : string;
  end;

  TProjectUnits = class(TList<TProjectUnit>)
  public
    function IsProjectUnit(UnitName : string) : boolean;
  end;

  TfrmUsesGrapher = class(TForm)
    mGraph: TMemo;
    btnOpenProject: TButton;
    OpenDialog1: TOpenDialog;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    procedure btnOpenProjectClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Label2Click(Sender: TObject);
    procedure Label3Click(Sender: TObject);
  private
    { Private declarations }
    ProjectUnits : TProjectUnits;
    procedure ProcessProjectUses(UsesNode: TSyntaxNode);
    procedure ParseUnit(ProjectUnit : TProjectUnit);
    procedure ParseUnits;
    procedure OpenURL(URL : String);
    function UnitNameToNode(UnitName : string) : string;
  public
    { Public declarations }
    function ParseFile(FileName : string) : TSyntaxNode;
  end;

var
  frmUsesGrapher: TfrmUsesGrapher;

implementation

uses
  DelphiAST, DelphiAST.Consts,
  ShellAPI;

{$R *.dfm}

procedure TfrmUsesGrapher.FormCreate(Sender: TObject);
begin
  ProjectUnits := TProjectUnits.Create;
end;

procedure TfrmUsesGrapher.FormDestroy(Sender: TObject);
begin
  ProjectUnits.Free;
end;

procedure TfrmUsesGrapher.btnOpenProjectClick(Sender: TObject);
var
  ProjectFileName : string;
  SyntaxTree: TSyntaxNode;
  UsesNode: TSyntaxNode;
begin
  if not OpenDialog1.Execute then
    Exit;

  ProjectUnits.Clear;
  mGraph.Clear;

  ProjectFileName := OpenDialog1.FileName;

  SetCurrentDir(ExtractFilePath(ProjectFileName));
    { unit file paths are releative to the project file }

  SyntaxTree := ParseFile(ProjectFileName);
  try
    UsesNode := SyntaxTree.FindNode([ntUses]);
    ProcessProjectUses(UsesNode);
  finally
    SyntaxTree.Free;
  end;
  ParseUnits;
end;

procedure TfrmUsesGrapher.OpenURL(URL: String);
begin
  ShellExecute(Handle, 'OPEN', PChar(URL), '', '', SW_SHOWDEFAULT);
end;

function TfrmUsesGrapher.ParseFile(FileName: string): TSyntaxNode;
var
  Builder: TPasSyntaxTreeBuilder;
  StringStream: TStringStream;
begin
  StringStream := TStringStream.Create;
  try
    Builder := TPasSyntaxTreeBuilder.Create;
    StringStream.LoadFromFile(FileName);
    Result := Builder.Run(StringStream);
  finally
    StringStream.Free;
  end;
end;

procedure TfrmUsesGrapher.ParseUnits;
var
  ProjectUnit: TProjectUnit;
begin
  mGraph.Lines.Add('digraph G {');
  for ProjectUnit in ProjectUnits do
    ParseUnit(ProjectUnit);
  mGraph.Lines.Add('}');
end;

procedure TfrmUsesGrapher.ParseUnit(ProjectUnit : TProjectUnit);
var
  SyntaxTree : TSyntaxNode;
  UnitSection: TSyntaxNode;
  Units : TSyntaxNode;
  aUnit: TSyntaxNode;
  UnitName : string;
begin
  SyntaxTree := ParseFile(ProjectUnit.FileName);

  for UnitSection in SyntaxTree.ChildNodes do
  begin { interface, implementation, initialization, and finalization }
    Units := UnitSection.FindNode([ntUses]);
      { all the units in the uses section }
    if Units = nil then
      Continue;  { no uses section }
    for aUnit in Units.ChildNodes do
    begin { for every unit in the uses }
      UnitName := aUnit.GetAttribute(anName);
      if ProjectUnits.IsProjectUnit(UnitName) then { Only interested in project units }
        mGraph.Lines.Add('  ' + UnitNameToNode(ProjectUnit.UnitName) + ' -> ' + UnitNameToNode(UnitName));
    end;
  end;
  SyntaxTree.Free;
end;

procedure TfrmUsesGrapher.ProcessProjectUses(UsesNode: TSyntaxNode);
var
  ChildNode: TSyntaxNode;
  ProjectUnit: TProjectUnit;
begin
  for ChildNode in UsesNode.ChildNodes do
  begin
    { find all the units that have a path (anPath) associated with them }
    ProjectUnit.UnitName := ChildNode.GetAttribute(anName);
    ProjectUnit.FileName := ChildNode.GetAttribute(anPath);
    if ProjectUnit.FileName <> '' then
      ProjectUnits.Add(ProjectUnit);
  end;
end;

function TfrmUsesGrapher.UnitNameToNode(UnitName: string): string;
begin
  result := UnitName.Replace('.', '_');
end;

{ TProjectUnits }

function TProjectUnits.IsProjectUnit(UnitName: string): boolean;
var
  ProjectUnit: TProjectUnit;
begin
  result := False;
  for ProjectUnit in Self do
  begin
    if SameText(ProjectUnit.UnitName, UnitName) then
      Exit(True)
  end;
end;


procedure TfrmUsesGrapher.Label2Click(Sender: TObject);
begin
  OpenURL('https://www.graphviz.org/');
end;

procedure TfrmUsesGrapher.Label3Click(Sender: TObject);
begin
  OpenURL('https://dreampuf.github.io/GraphvizOnline');
end;

end.
