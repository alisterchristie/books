object frmUsesGrapher: TfrmUsesGrapher
  Left = 0
  Top = 0
  Caption = 'Project Uses Grapher'
  ClientHeight = 307
  ClientWidth = 533
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    533
    307)
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel
    Left = 160
    Top = 8
    Width = 118
    Height = 16
    Caption = 'Produce Graph using'
  end
  object Label2: TLabel
    Left = 284
    Top = 8
    Width = 75
    Height = 16
    Cursor = crHandPoint
    Caption = 'Graphviz App'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsUnderline]
    ParentFont = False
    OnClick = Label2Click
  end
  object Label3: TLabel
    Left = 284
    Top = 25
    Width = 89
    Height = 16
    Cursor = crHandPoint
    Caption = 'Graphviz Online'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsUnderline]
    ParentFont = False
    OnClick = Label3Click
  end
  object mGraph: TMemo
    Left = 8
    Top = 47
    Width = 517
    Height = 250
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    ExplicitWidth = 365
  end
  object btnOpenProject: TButton
    Left = 8
    Top = 8
    Width = 137
    Height = 25
    Caption = 'Open Project'
    TabOrder = 1
    OnClick = btnOpenProjectClick
  end
  object OpenDialog1: TOpenDialog
    Filter = 'Delphi Projects|*.dpr|All Files|*.*'
    Left = 176
    Top = 136
  end
end
