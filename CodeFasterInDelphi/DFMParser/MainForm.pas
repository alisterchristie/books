unit MainForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uDFMParser, Vcl.StdCtrls, Vcl.ComCtrls;

type
  TfrmMigrateDFMs = class(TForm)
    b1: TButton;
    bDFMs: TButton;
    lstDFMFiles: TListBox;
    procedure b1Click(Sender: TObject);
    procedure bDFMsClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ProcessReportObject(Tree: TDfmObject);
    function ProcessReportFile(InFileName, OutFileName : string) : boolean;
    procedure ProcessDFMObject(DFMObj : TDfmObject);
    procedure ProccessDFMFile(FileName : string);
  end;

var
  frmMigrateDFMs: TfrmMigrateDFMs;

implementation

uses
  System.IOUtils,
  System.Types,
  System.StrUtils;

{$R *.dfm}

procedure TfrmMigrateDFMs.ProcessReportObject(Tree: TDfmObject);
var
  I: Integer;
  p : TDfmProperty;
begin
  for I := 0 to Tree.DfmPropertyCount-1 do
  begin
    p := Tree.DfmProperty[i];
    if SameText(p.PropertyName, 'DatabaseName') then
    begin
      p.PropertyName := 'Connection';
      p.PropertyType := ptIdent;
      p.StringValue := 'dmFireDACStandard.Connection';
    end;
  end;
  if SameText(Tree.DfmClassName, 'TQuery') then
    Tree.DfmClassName := 'TFDQuery';
  if SameText(Tree.DfmClassName, 'TTable') then
    Tree.DfmClassName := 'TFDTable';
    for I := 0 to Tree.OwnedObjectCount-1 do
      ProcessReportObject(Tree.OwnedObject[i]);
end;

procedure TfrmMigrateDFMs.b1Click(Sender: TObject);
var
  Files: TStringDynArray;
  Errors : TStringList;
  AFile : string;
begin
  Errors := TStringList.Create;
  Files := TDirectory.GetFiles('c:\spartner\reports\oreports\', '*.qr2');
  for AFile in Files do
  begin
    try
      ProcessReportFile(AFile, AFile); //replace files
    except
      Errors.Add(AFile);
    end;
  end;
  if Errors.Count > 0 then
    ShowMessage('Files that failed:'#10 + Errors.Text)
  else
    ShowMessage('Done, All Successful');
end;


procedure TfrmMigrateDFMs.bDFMsClick(Sender: TObject);
var
  FileName: string;
begin
   for FileName in lstDFMFiles.Items do
   begin
     ProccessDFMFile(FileName);
   end;
end;

procedure TfrmMigrateDFMs.ProccessDFMFile(FileName: string);
var
  InStream, OutStream : TMemoryStream;
  Tree: TDfmTree;
begin
  InStream := TMemoryStream.Create;
  InStream.LoadFromFile(FileName);
  OutStream := TMemoryStream.Create;

  Tree := TDfmTree.Create;
  ObjectTextToTree(InStream, Tree);

  ProcessDFMObject(Tree);

  ObjectTreeToText(Tree, OutStream);
  Tree.Free;

  OutStream.SaveToFile(FileName);
  OutStream.Free;
  InStream.Free;
end;

procedure TfrmMigrateDFMs.ProcessDFMObject(DFMObj: TDfmObject);
var
  I: Integer;
  p : TDfmProperty;
begin
  if MatchText(DFMObj.DfmClassName, ['TFDQuery', 'TFDTable']) then
  begin
    for I := 0 to DFMObj.DfmPropertyCount-1 do
    begin
      p := DFMObj.DfmProperty[i];
      if SameText(p.PropertyName, 'ConnectionName') then
      begin
        //we want to change all ConnectionNames to a Connection
        p.PropertyName := 'Connection';
        p.PropertyType := ptIdent;
        p.StringValue := 'dmFireDACStandard.Connection';
      end;
    end;
    for I := DFMObj.DfmPropertyCount-1 downto 0 do
    begin
      //remove properties that don't exist on TFDQuery or TFDTable
      p := DFMObj.DfmProperty[i];
      if MatchText(p.PropertyName, ['FieldDefs', 'StoreDefs', 'IndexDefs']) then
        DFMObj.RemoveDfmProperty(p);
    end;
  end
  else
  if MatchText(DFMObj.DfmClassName, ['TQRDesignFDACQueryInterface', 'TQRDesignFDACTableInterface']) then
  begin
    for I := DFMObj.DfmPropertyCount-1 downto 0 do
    begin
      p := DFMObj.DfmProperty[i];
      if MatchText(p.PropertyName, ['UpdateOptions.RequestLive', 'FetchOptions.Unidirectional']) then
        DFMObj.RemoveDfmProperty(p);
    end;
  end
  else
  begin
    //recurse for all children
    for I := 0 to DFMObj.OwnedObjectCount-1 do
      ProcessDFMObject(DFMObj.OwnedObject[i]);
  end;
end;

function TfrmMigrateDFMs.ProcessReportFile(InFileName, OutFileName: string): boolean;
var
  InStream, OutStream : TMemoryStream;
  Tree: TDfmTree;
  I: Integer;
begin
  InStream := TMemoryStream.Create;
  InStream.LoadFromFile(InFileName);
  OutStream := TMemoryStream.Create;

  for I := 1 to 4 do
  begin
    Tree := TDfmTree.Create;
    ObjectTextToTree(InStream, Tree);

    ProcessReportObject(Tree);

    ObjectTreeToText(Tree, OutStream);
    Tree.Free;
  end;

  OutStream.SaveToFile(OutFileName);
  OutStream.Free;
  InStream.Free;
  result := True; //want to return true if the file is modified
end;


end.
