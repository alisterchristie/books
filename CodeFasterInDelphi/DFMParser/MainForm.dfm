object frmMigrateDFMs: TfrmMigrateDFMs
  Left = 0
  Top = 0
  Caption = 'frmMigrateDFMs'
  ClientHeight = 260
  ClientWidth = 395
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object b1: TButton
    Left = 8
    Top = 8
    Width = 137
    Height = 25
    Caption = 'Migrate Quick Reports'
    TabOrder = 0
    OnClick = b1Click
  end
  object bDFMs: TButton
    Left = 168
    Top = 8
    Width = 121
    Height = 25
    Caption = 'Migrate DFMs'
    TabOrder = 1
    OnClick = bDFMsClick
  end
  object lstDFMFiles: TListBox
    Left = 8
    Top = 39
    Width = 353
    Height = 194
    ItemHeight = 13
    Items.Strings = (
      'C:\Delphiapps\myapps\Rezyprog\actData.dfm'
      'C:\Delphiapps\myapps\Rezyprog\AdvertTemplateData.dfm'
      'C:\Delphiapps\myapps\Rezyprog\AgentXferXlation.dfm'
      'C:\Delphiapps\myapps\Rezyprog\BuyerTransfer.dfm'
      'C:\Delphiapps\myapps\Rezyprog\ChattelsEditor.dfm'
      'C:\Delphiapps\myapps\Rezyprog\ContEntryDS.dfm'
      'C:\Delphiapps\myapps\Rezyprog\ContListDS.dfm'
      'C:\Delphiapps\myapps\Rezyprog\ContLkup.dfm'
      'C:\Delphiapps\myapps\Rezyprog\CopyListing.dfm'
      'C:\Delphiapps\myapps\Rezyprog\DatabaseLabelEditor.dfm'
      'C:\Delphiapps\myapps\Rezyprog\DatabaseModule.dfm'
      'C:\Delphiapps\myapps\Rezyprog\EmailWizard.dfm'
      'C:\Delphiapps\myapps\Rezyprog\FFEditor.dfm'
      'C:\Delphiapps\myapps\Rezyprog\ILSData.dfm'
      'C:\Delphiapps\myapps\Rezyprog\ImportOpenHomeRegister.dfm'
      'C:\Delphiapps\myapps\Rezyprog\InvoicingCheckOff.dfm'
      'C:\Delphiapps\myapps\Rezyprog\LeadersXfer.dfm'
      'C:\Delphiapps\myapps\Rezyprog\ListEnqDS.dfm'
      'C:\Delphiapps\myapps\Rezyprog\ListingCategoryData.dfm'
      'C:\Delphiapps\myapps\Rezyprog\ListingQuickEntry.dfm'
      'C:\Delphiapps\myapps\Rezyprog\listlistDS.dfm'
      'C:\Delphiapps\myapps\Rezyprog\LocateVendorListing.dfm'
      'C:\Delphiapps\myapps\Rezyprog\MaintainAreas.dfm'
      'C:\Delphiapps\myapps\Rezyprog\MaintainMisc.dfm'
      'C:\Delphiapps\myapps\Rezyprog\MaintCodeData.dfm'
      'C:\Delphiapps\myapps\Rezyprog\MaintSp.dfm'
      'C:\Delphiapps\myapps\Rezyprog\MaintSubs.dfm'
      'C:\Delphiapps\myapps\Rezyprog\OHContDS.dfm'
      'C:\Delphiapps\myapps\Rezyprog\OSLImporterData.dfm'
      'C:\Delphiapps\myapps\Rezyprog\PhotosOnline.dfm'
      'C:\Delphiapps\myapps\Rezyprog\PropertyInspectionData.dfm'
      'C:\Delphiapps\myapps\Rezyprog\ReadinDS.dfm'
      'C:\Delphiapps\myapps\Rezyprog\RECO_Data.dfm'
      'C:\Delphiapps\myapps\Rezyprog\RepFormSel.dfm'
      'C:\Delphiapps\myapps\Rezyprog\ReSaveProperty.dfm'
      'C:\Delphiapps\myapps\Rezyprog\ReUploadPhotos.dfm'
      'C:\Delphiapps\myapps\Rezyprog\RezyGlobalData.dfm'
      'C:\Delphiapps\myapps\Rezyprog\RezyStdListDataSet.dfm'
      'C:\Delphiapps\myapps\Rezyprog\RptDS.dfm'
      'C:\Delphiapps\myapps\Rezyprog\SaleHistDS.dfm'
      'C:\Delphiapps\myapps\Rezyprog\SaleLookup.dfm'
      'C:\Delphiapps\myapps\Rezyprog\SaleWizard.dfm'
      'C:\Delphiapps\myapps\Rezyprog\SelectDBCode.dfm'
      'C:\Delphiapps\myapps\Rezyprog\SolicitorLookup.ViewModel.dfm'
      'C:\Delphiapps\myapps\Rezyprog\SPReadInData.dfm'
      'C:\Delphiapps\myapps\Rezyprog\StdContDS.dfm'
      'C:\Delphiapps\myapps\Rezyprog\SuburbTranslation.dfm'
      'C:\Delphiapps\myapps\Rezyprog\SysMaintDS.dfm'
      'C:\Delphiapps\myapps\Rezyprog\TaskTemplatesData.dfm'
      'C:\Delphiapps\myapps\Rezyprog\TipWin.dfm'
      'C:\Delphiapps\myapps\Rezyprog\TipWinEditor.dfm'
      'C:\Delphiapps\myapps\Rezyprog\ToDoDS.dfm'
      'C:\Delphiapps\myapps\Rezyprog\TradeMeAPIUpdater.dfm'
      'C:\Delphiapps\myapps\Rezyprog\VisionPhotoByReferenceCopier.dfm'
      'C:\Delphiapps\myapps\Rezyprog\VNote.dfm'
      'C:\Delphiapps\inc\DataExplorer.dfm'
      'C:\Delphiapps\myapps\RezyCommon\CategorySelector.dfm'
      'C:\Delphiapps\myapps\RezyCommon\RTE.dfm'
      'C:\Delphiapps\myapps\RezyCommon\SelectReportsBaseClass.dfm'
      'C:\Delphiapps\myapps\rezysale\ActivityDM.dfm'
      'C:\Delphiapps\myapps\rezysale\AddsDS.dfm'
      'C:\Delphiapps\myapps\rezysale\Ads4PubListingSelect.dfm'
      'C:\Delphiapps\myapps\rezysale\adsDS.dfm'
      'C:\Delphiapps\myapps\rezysale\AdsRptDS.dfm'
      'C:\Delphiapps\myapps\rezysale\BulkRecoveryEditor.dfm'
      'C:\Delphiapps\myapps\rezysale\ComEntry.dfm'
      'C:\Delphiapps\myapps\rezysale\CommissionRateDefaults.dfm'
      'C:\Delphiapps\myapps\rezysale\CommissionRecoveryAnalyser.dfm'
      'C:\Delphiapps\myapps\rezysale\ContQuickEntry.dfm'
      'C:\Delphiapps\myapps\rezysale\CreateAdvert.dfm'
      'C:\Delphiapps\myapps\rezysale\DebtorsData.dfm'
      'C:\Delphiapps\myapps\rezysale\EnterNewSale.dfm'
      'C:\Delphiapps\myapps\rezysale\Form9.dfm'
      'C:\Delphiapps\myapps\rezysale\galleryDS.dfm'
      'C:\Delphiapps\myapps\rezysale\GLExport.dfm'
      'C:\Delphiapps\myapps\rezysale\InvoicingDataModule.dfm'
      'C:\Delphiapps\myapps\rezysale\MaintainSystemFile.dfm'
      'C:\Delphiapps\myapps\rezysale\ModifiedSalesPersonReport.dfm'
      'C:\Delphiapps\myapps\rezysale\OfficeStdDataSet.dfm'
      'C:\Delphiapps\myapps\rezysale\PBAwardsCalculator.dfm'
      'C:\Delphiapps\myapps\rezysale\PBReturn.dfm'
      'C:\Delphiapps\myapps\rezysale\RemaxReturn.dfm'
      'C:\Delphiapps\myapps\rezysale\ReportDataExtra.dfm'
      'C:\Delphiapps\myapps\rezysale\ReportDataModule.dfm'
      'C:\Delphiapps\myapps\rezysale\SaleDS.dfm'
      'C:\Delphiapps\myapps\rezysale\SalesPeopleDataModule.dfm'
      'C:\Delphiapps\myapps\rezysale\TrustDataModule.dfm'
      'C:\Delphiapps\myapps\rezysale\TrustEntryScreen.dfm'
      'C:\Delphiapps\myapps\rezysale\TrustReconcileDS.dfm'
      'C:\Delphiapps\myapps\rezysale\TrustsDS.dfm'
      'C:\Delphiapps\myapps\rezysale\YearOnYear.dfm'
      'C:\Delphiapps\myobj\R_LOOKUP_FRM.dfm')
    TabOrder = 2
  end
end
