program DFMParserExample;

uses
  Vcl.Forms,
  System.SysUtils,
  MainForm in 'MainForm.pas' {frmMigrateDFMs},
  uDFMParser in 'uDFMParser.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMigrateDFMs, frmMigrateDFMs);
  if FindCmdLineSwitch('DFM') then
  begin
    frmMigrateDFMs.bDFMsClick(nil);
    Application.Terminate;
    Exit;
  end;
  Application.Run;
end.
