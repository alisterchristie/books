object frmRTTIWrite: TfrmRTTIWrite
  Left = 0
  Top = 0
  Caption = 'frmRTTIWrite'
  ClientHeight = 206
  ClientWidth = 536
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object FlowPanel1: TFlowPanel
    Left = 0
    Top = 0
    Width = 536
    Height = 206
    Align = alClient
    Caption = 'FlowPanel1'
    TabOrder = 0
    ExplicitWidth = 504
    ExplicitHeight = 164
    object btn1: TButton
      Left = 1
      Top = 1
      Width = 75
      Height = 25
      Caption = 'btn1'
      TabOrder = 0
    end
    object RadioButton1: TRadioButton
      Left = 76
      Top = 1
      Width = 113
      Height = 17
      Caption = 'RadioButton1'
      TabOrder = 1
    end
    object CheckBox1: TCheckBox
      Left = 189
      Top = 1
      Width = 97
      Height = 17
      Caption = 'CheckBox1'
      TabOrder = 2
    end
    object Memo1: TMemo
      Left = 286
      Top = 1
      Width = 185
      Height = 89
      Lines.Strings = (
        'Memo1')
      TabOrder = 3
    end
    object Panel1: TPanel
      Left = 1
      Top = 90
      Width = 185
      Height = 41
      Caption = 'Panel1'
      TabOrder = 4
    end
    object Edit1: TEdit
      Left = 186
      Top = 90
      Width = 121
      Height = 21
      TabOrder = 5
      Text = 'Edit1'
    end
    object Label1: TLabel
      Left = 307
      Top = 90
      Width = 31
      Height = 13
      Caption = 'Label1'
    end
    object BitBtn1: TBitBtn
      Left = 338
      Top = 90
      Width = 75
      Height = 25
      Caption = 'BitBtn1'
      TabOrder = 6
    end
    object SpeedButton1: TSpeedButton
      Left = 413
      Top = 90
      Width = 58
      Height = 22
    end
    object StaticText1: TStaticText
      Left = 471
      Top = 90
      Width = 59
      Height = 17
      Caption = 'StaticText1'
      TabOrder = 7
    end
    object ButtonedEdit1: TButtonedEdit
      Left = 1
      Top = 131
      Width = 121
      Height = 21
      TabOrder = 8
      Text = 'ButtonedEdit1'
    end
    object ComboBoxEx1: TComboBoxEx
      Left = 122
      Top = 131
      Width = 145
      Height = 22
      ItemsEx = <>
      TabOrder = 9
      Text = 'ComboBoxEx1'
    end
    object ComboBox1: TComboBox
      Left = 267
      Top = 131
      Width = 145
      Height = 21
      TabOrder = 10
      Text = 'ComboBox1'
    end
  end
end
