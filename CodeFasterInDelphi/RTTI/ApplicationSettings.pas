unit ApplicationSettings;

interface

uses IniFiles;

type
  TAppSettings = class
  private
    ini : TIniFile;
    function GetDefaultFolder: string;
    procedure SetDefaultFolder(const Value: string);
  public
    property DefaultFolder: string read GetDefaultFolder write SetDefaultFolder;
  end;

implementation

function TAppSettings.GetDefaultFolder: string;
begin
  result := ini.ReadString('settings', 'DefaultFolder', '');
end;

procedure TAppSettings.SetDefaultFolder(const Value: string);
begin
  ini.WriteString('settings', 'DefaultFolder', Value);
end;

end.
