object Form11: TForm11
  Left = 0
  Top = 0
  Caption = 'RTTI Read Properties'
  ClientHeight = 308
  ClientWidth = 660
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    660
    308)
  PixelsPerInch = 96
  TextHeight = 13
  object btnShowProperties: TButton
    Left = 8
    Top = 8
    Width = 105
    Height = 25
    Caption = 'Show Properties'
    TabOrder = 0
    OnClick = btnShowPropertiesClick
  end
  object Memo1: TMemo
    Left = 8
    Top = 39
    Width = 642
    Height = 258
    Anchors = [akLeft, akTop, akRight, akBottom]
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    Lines.Strings = (
      'Memo1')
    ParentFont = False
    ScrollBars = ssBoth
    TabOrder = 1
    WordWrap = False
  end
  object cbDeclaredOnly: TCheckBox
    Left = 136
    Top = 12
    Width = 145
    Height = 17
    Caption = 'Only on Current Classs'
    TabOrder = 2
  end
end
