unit RttiRead;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm11 = class(TForm)
    btnShowProperties: TButton;
    Memo1: TMemo;
    cbDeclaredOnly: TCheckBox;
    procedure btnShowPropertiesClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form11: TForm11;

implementation

uses
  System.RTTI;

{$R *.dfm}

procedure TForm11.btnShowPropertiesClick(Sender: TObject);
var
  R: TRttiContext;
  Props: TArray<TRttiProperty>;
  Prop : TRttiProperty;
begin
  memo1.Clear;
  R := TRttiContext.Create;
  if cbDeclaredOnly.Checked then //on current class only
    Props := R.GetType(Sender.ClassType).GetDeclaredProperties
  else //properties on class and ancestors
    Props := R.GetType(Sender.ClassType).GetProperties;
  //also .GetMethods, .GetFields, .GetAttributes etc.

  //List all the properties in the memo
  for Prop in Props do
  begin
    try
      Memo1.Lines.Add(
         Prop.Parent.Name + '.' +
         Prop.Name + ' : ' +
         Prop.PropertyType.ToString + ' = ' +
         Prop.GetValue(Sender).ToString);
    except
      on E: Exception do
        Memo1.Lines.Add(Prop.Name + ' generated an exception (' + E.Message + ')');
    end;
  end;
end;

end.
