unit RTTIWriteForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls,
  Vcl.ExtCtrls, Vcl.Buttons;

type
  TfrmRTTIWrite = class(TForm)
    FlowPanel1: TFlowPanel;
    btn1: TButton;
    RadioButton1: TRadioButton;
    CheckBox1: TCheckBox;
    Memo1: TMemo;
    Panel1: TPanel;
    Edit1: TEdit;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    SpeedButton1: TSpeedButton;
    StaticText1: TStaticText;
    ButtonedEdit1: TButtonedEdit;
    ComboBoxEx1: TComboBoxEx;
    ComboBox1: TComboBox;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRTTIWrite: TfrmRTTIWrite;

implementation

uses
  System.RTTI;

{$R *.dfm}

procedure TfrmRTTIWrite.FormShow(Sender: TObject);
var
  R: TRttiContext;
  P: TRttiProperty;
  I: Integer;
  C : TComponent;
  v : TValue;
begin
  R := TRttiContext.Create;
  for I := 0 to ComponentCount-1 do
  begin
    c := Components[i];
    P := R.GetType(c.ClassType).GetProperty('Caption');
    if p = nil then
      P := R.GetType(c.ClassType).GetProperty('Tag');
    if p <> nil then
    begin
      case p.PropertyType.TypeKind of
        tkInteger, tkInt64: p.SetValue(c, TValue.From(-1));
        tkChar, tkWChar: p.SetValue(c, TValue.From('X'));
        tkFloat: p.SetValue(c, TValue.From(-1.0));
        tkString, tkLString, tkWString, tkUString: p.SetValue(c, TValue.From('*REDACTED*'));
        {  ... }
      end;
    end;
  end;
end;

end.
