unit PrimeForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.WinXCtrls, System.Threading,
  Vcl.ExtCtrls;

type
  TfrmPrimes = class(TForm)
    btnNoParallel: TButton;
    mmoTwins: TMemo;
    btnBackground: TButton;
    btnParallel: TButton;
    ActivityIndicator1: TActivityIndicator;
    btnParallelFor: TButton;
    cbFinalTwinPrimeOnly: TCheckBox;
    procedure btnNoParallelClick(Sender: TObject);
    procedure btnBackgroundClick(Sender: TObject);
    procedure btnParallelClick(Sender: TObject);
    procedure btnParallelForClick(Sender: TObject);
  private
    function IsPrime(const x : integer) : boolean;
    procedure TwinPrimes(LowBound, HighBound: integer; Output: TStrings);
    procedure TwinPrimesParallel(LowBound, HighBound : integer; Output : TStrings);
    procedure ParallelTasks(Output: TStrings);
    function CreateTask(x: integer; Output : TStrings): ITask;
    procedure ShowResult(sl : TStringList);
  public
  end;

var
  frmPrimes: TfrmPrimes;

implementation

uses
  Math,
  System.Diagnostics;

{$R *.dfm}

const
  TaskCount = 5;
  MaxPrime = 10000000;

procedure TfrmPrimes.btnBackgroundClick(Sender: TObject);
begin
  TTask.Create(procedure
    var
      sl : TStringList;
      sw : TStopwatch;
    begin
      sw := TStopWatch.StartNew;
      sl := TStringList.Create;
      TwinPrimes(2, MaxPrime, sl);
      sl.Add(sw.Elapsed.TotalMilliseconds.ToString);
      ShowResult(sl);
      sl.Free;
    end
  ).Start;
end;

procedure TfrmPrimes.btnNoParallelClick(Sender: TObject);
var
  sl : TStringList;
  sw : TStopwatch;
begin
  sw := TStopWatch.StartNew;
  sl := TStringList.Create;
  TwinPrimes(2, MaxPrime, sl);
  sl.Add(sw.Elapsed.TotalMilliseconds.ToString);
  ShowResult(sl);
  sl.Free;
end;

procedure TfrmPrimes.btnParallelForClick(Sender: TObject);
begin
  TTask.Create(procedure
    var
      sl : TStringList;
      sw : TStopwatch;
    begin
      sw := TStopWatch.StartNew;
      sl := TStringList.Create;
      TwinPrimesParallel(2, MaxPrime, sl);
      sl.Add(sw.Elapsed.TotalMilliseconds.ToString);
      ShowResult(sl);
      sl.Free;
    end
  ).Start;
end;

procedure TfrmPrimes.btnParallelClick(Sender: TObject);
begin
  TTask.Create(procedure
    var
      sl : TStringList;
      sw : TStopwatch;
    begin
      sw := TStopWatch.StartNew;
      sl := TStringList.Create;
      ParallelTasks(sl);
      sl.Add(sw.Elapsed.TotalMilliseconds.ToString);
      ShowResult(sl);
      sl.Free;
    end
  ).Start;
end;

procedure TfrmPrimes.ParallelTasks(Output: TStrings);
var
  Tasks : TArray<ITask>;
  Lists : TArray<TStringList>;
  i : Integer;
begin
  SetLength(Lists, TaskCount);
  for i := 0 to TaskCount-1 do
    Lists[i] := TStringList.Create;

  SetLength(Tasks, TaskCount);
  for I := 0 to TaskCount-1 do
    Tasks[i] := CreateTask(i, Lists[i]).Start;

  TTask.WaitForAll(Tasks);
  for i := 0 to TaskCount-1 do
  begin
    OutPut.AddStrings(Lists[i]);
    Lists[i].Free;
  end;
end;

procedure TfrmPrimes.ShowResult(sl : TStringList);
begin
  TThread.Synchronize(nil,
    procedure
    begin
      if cbFinalTwinPrimeOnly.Checked then
      begin
        mmoTwins.Clear;
        if sl.Count <= 2 then
          Exit;
        mmoTwins.Lines.Add(sl[sl.Count-2]); { final result }
        mmoTwins.Lines.Add(sl[sl.Count-1]); { calculation time }
      end
      else
        mmoTwins.Lines.Text := sl.Text;
    end
  );
end;

function TfrmPrimes.CreateTask(x: integer; Output : TStrings): ITask;
begin
  Result := TTask.Create(procedure
    var
       LowBound, HighBound : integer;
    begin
      LowBound := (MaxPrime * x div TaskCount) + 2;
      HighBound := (MaxPrime * (x+1) div TaskCount) + 1;
      TwinPrimes(LowBound, HighBound, Output);
    end
  );
end;

function TfrmPrimes.IsPrime(const x: integer): boolean;
var
  I: Integer;
  MaxValue : integer;
begin
  MaxValue := Round(Sqrt(x));
  for I := 2 to MaxValue do
  begin
    if (x mod i) = 0 then
      exit(False);
  end;
  result := True;
end;

procedure TfrmPrimes.TwinPrimes(LowBound, HighBound: integer;
  Output: TStrings);
var
  I : integer;
begin
  for I := LowBound to HighBound do
  begin
    if IsPrime(i) and IsPrime(i+2) then
      OutPut.Add(i.ToString + ', ' + (i+2).ToString);
  end;
end;

procedure TfrmPrimes.TwinPrimesParallel(LowBound, HighBound: integer;
  Output: TStrings);
begin
  TParallel.For(LowBound, HighBound, procedure(i : integer)
  begin
    if IsPrime(i) and IsPrime(i+2) then
    begin
      System.TMonitor.Enter(OutPut);
      OutPut.Add(i.ToString + ', ' + (i+2).ToString);
      System.TMonitor.Exit(OutPut);
    end;
  end);
end;

end.
