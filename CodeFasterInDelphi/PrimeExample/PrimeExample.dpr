program PrimeExample;

uses
  Vcl.Forms,
  PrimeForm in 'PrimeForm.pas' {frmPrimes};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmPrimes, frmPrimes);
  Application.Run;
end.
