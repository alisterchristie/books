object frmPrimes: TfrmPrimes
  Left = 0
  Top = 0
  Caption = 'Twin Primes'
  ClientHeight = 135
  ClientWidth = 299
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    299
    135)
  PixelsPerInch = 96
  TextHeight = 13
  object btnNoParallel: TButton
    Left = 8
    Top = 8
    Width = 75
    Height = 25
    Caption = 'No Parallel'
    TabOrder = 0
    OnClick = btnNoParallelClick
  end
  object mmoTwins: TMemo
    Left = 134
    Top = 8
    Width = 157
    Height = 97
    Anchors = [akLeft, akTop, akRight, akBottom]
    Lines.Strings = (
      'mmoTwins')
    TabOrder = 1
  end
  object btnBackground: TButton
    Left = 8
    Top = 39
    Width = 75
    Height = 25
    Caption = 'Background'
    TabOrder = 2
    OnClick = btnBackgroundClick
  end
  object btnParallel: TButton
    Left = 8
    Top = 70
    Width = 75
    Height = 25
    Caption = 'Parallel Tasks'
    TabOrder = 3
    OnClick = btnParallelClick
  end
  object ActivityIndicator1: TActivityIndicator
    Left = 96
    Top = 8
    Animate = True
  end
  object btnParallelFor: TButton
    Left = 8
    Top = 101
    Width = 75
    Height = 25
    Caption = 'Parallel For'
    TabOrder = 5
    OnClick = btnParallelForClick
  end
  object cbFinalTwinPrimeOnly: TCheckBox
    Left = 134
    Top = 110
    Width = 161
    Height = 17
    Anchors = [akLeft, akBottom]
    Caption = 'Show Only Final Twin Prime'
    TabOrder = 6
  end
end
