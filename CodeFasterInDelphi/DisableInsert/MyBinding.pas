unit MyBinding;

interface

procedure Register;

implementation

uses Windows, Classes, SysUtils, ToolsAPI, Vcl.Menus;

type
  TLearnDelphiKeyBinding = class(TNotifierObject, IOTAKeyboardBinding)
  private
    procedure DoNothing(const Context: IOTAKeyContext; KeyCode: TShortCut; var BindingResult: TKeyBindingResult);

  public
    function GetBindingType: TBindingType;
    function GetDisplayName: string;
    function GetName: string;
    procedure BindKeyboard(const BindingServices: IOTAKeyBindingServices);
  end;

var
  LearnDelphiKeyBindingIndex : integer = 0;

procedure Register;
begin
  LearnDelphiKeyBindingIndex := (BorlandIDEServices as IOTAKeyBoardServices).AddKeyboardBinding(TLearnDelphiKeyBinding.Create);
end;

procedure TLearnDelphiKeyBinding.BindKeyboard(const BindingServices: IOTAKeyBindingServices);
begin
  BindingServices.AddKeyBinding([ShortCut(VK_INSERT, [])], DoNothing, nil);
end;

function TLearnDelphiKeyBinding.GetBindingType: TBindingType;
begin
  Result := btPartial;
end;

function TLearnDelphiKeyBinding.GetDisplayName: string;
begin
  Result := 'Disable Insert';
end;

function TLearnDelphiKeyBinding.GetName: string;
begin
  Result := 'LearnDelphi.DisableInsert';
end;

procedure TLearnDelphiKeyBinding.DoNothing(const Context: IOTAKeyContext;
  KeyCode: TShortCut; var BindingResult: TKeyBindingResult);
begin
  BindingResult := krHandled;
end;

initialization
finalization
  if LearnDelphiKeyBindingIndex > 0 then
    (BorlandIDEServices as IOTAKeyboardServices).RemoveKeyboardBinding(LearnDelphiKeyBindingIndex);

end.
