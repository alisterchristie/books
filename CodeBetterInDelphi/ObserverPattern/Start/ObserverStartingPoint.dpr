program ObserverStartingPoint;

uses
  Vcl.Forms,
  SubjectForm in 'SubjectForm.pas' {fSubject},
  ObserverForm in 'ObserverForm.pas' {fObserver};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmSubject, frmSubject);
  Application.Run;
end.
