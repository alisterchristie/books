unit ObserverForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs;

type
  TfrmObserver = class(TForm)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Update(color : TColor);
  end;

var
  frmObserver: TfrmObserver;

implementation

{$R *.dfm}

uses
  SubjectForm;

{ TfObserver }

procedure TfrmObserver.Update(color : TColor);
begin
  Self.color := Color;
end;

end.
