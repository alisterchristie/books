unit SubjectForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ColorGrd, Vcl.StdCtrls,
  System.Generics.Collections, ObserverForm;

type
  TObserverList = TList<TfrmObserver>;

  TfrmSubject = class(TForm)
    btnNewForm: TButton;
    cgObserverColor: TColorGrid;
    procedure btnNewFormClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure cgObserverColorChange(Sender: TObject);
  private
    { Private declarations }
    Observers : TObserverList;
  public
    { Public declarations }

  end;

var
  frmSubject: TfrmSubject;

implementation

{$R *.dfm}

procedure TfrmSubject.btnNewFormClick(Sender: TObject);
begin
  var f := TfrmObserver.Create(Self);
  Observers.Add(f);
end;

procedure TfrmSubject.cgObserverColorChange(Sender: TObject);
begin
  for var Observer in Observers do
    Observer.Update(cgObserverColor.ForegroundColor);
end;

procedure TfrmSubject.FormCreate(Sender: TObject);
begin
  Observers := TObserverList.Create;
end;

procedure TfrmSubject.FormDestroy(Sender: TObject);
begin
  Observers.Free;
end;

end.
