object frmSubject: TfrmSubject
  Left = 0
  Top = 0
  AutoSize = True
  Caption = 'Subject Form'
  ClientHeight = 243
  ClientWidth = 223
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Padding.Left = 10
  Padding.Top = 10
  Padding.Right = 10
  Padding.Bottom = 10
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  TextHeight = 13
  object btnNewForm: TButton
    Left = 10
    Top = 10
    Width = 203
    Height = 41
    Caption = 'Create Form'
    TabOrder = 0
    OnClick = btnNewFormClick
  end
  object cgObserverColor: TColorGrid
    Left = 13
    Top = 57
    Width = 200
    Height = 176
    BackgroundEnabled = False
    TabOrder = 1
    OnChange = cgObserverColorChange
  end
end
