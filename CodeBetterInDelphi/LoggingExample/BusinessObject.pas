﻿unit BusinessObject;

interface

uses
  LoggerInterface;

type
  TBusinessObject = class
  protected
    Log : ILogger;
  public
    procedure DoBusinessThing;
    constructor Create(Log : ILogger);
    destructor Destroy; override;
  end;

implementation

uses
  System.SysUtils;

{ TBusinessObject }

constructor TBusinessObject.Create(Log : ILogger);
begin
  inherited Create;
  Self.Log := Log;
  Sleep(200);
  Log.Log('Create TBusinessObject');
end;

destructor TBusinessObject.Destroy;
begin
  Sleep(200);
  Log.Log('Destroy TBusinessObject');
  inherited;
end;

procedure TBusinessObject.DoBusinessThing;
begin
  Log.Log('Begin TBusinessObject.DoBusinessThing');
  Sleep(333);
  Log.Log('End TBusinessObject.DoBusinessThing');
end;

end.
