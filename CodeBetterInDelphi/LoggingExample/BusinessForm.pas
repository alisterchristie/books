unit BusinessForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, BusinessObject,
  StringsLogger;

type
  TfrmBusiness = class(TForm)
    mLog: TMemo;
    Label1: TLabel;
    bDoBusinessThing: TButton;
    procedure bDoBusinessThingClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmBusiness: TfrmBusiness;

implementation

{$R *.dfm}

procedure TfrmBusiness.bDoBusinessThingClick(Sender: TObject);
var
  BO: TBusinessObject;
begin
   BO := TBusinessObject.Create;
   try
     BO.DoBusinessThing;
   finally
     BO.Free;
   end;
end;

procedure TfrmBusiness.FormCreate(Sender: TObject);
begin
  Log := TStringsLogger.Create(mLog.Lines);
end;

procedure TfrmBusiness.FormDestroy(Sender: TObject);
begin
  Log.Free;
end;

end.
