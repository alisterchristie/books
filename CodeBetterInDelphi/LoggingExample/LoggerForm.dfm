object frmBusiness: TfrmBusiness
  Left = 0
  Top = 0
  Caption = 'Business Form'
  ClientHeight = 138
  ClientWidth = 363
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 17
    Width = 17
    Height = 13
    Caption = 'Log'
  end
  object mLog: TMemo
    Left = 8
    Top = 36
    Width = 345
    Height = 93
    TabOrder = 0
  end
  object bDoBusinessThing: TButton
    Left = 247
    Top = 5
    Width = 106
    Height = 25
    Caption = 'Do Business Thing'
    TabOrder = 1
    OnClick = bDoBusinessThingClick
  end
end
