unit StringsLogger;

interface

uses
  System.Classes;

type
  TStringsLogger = class(TObject)
  protected
    StringsLog : TStrings;
  public
    constructor Create(strings : TStrings);
    procedure Log(s : string);
  end;

var
  Log : TStringsLogger;

implementation

{ TStringsLogger }

constructor TStringsLogger.Create(strings : TStrings);
begin
  inherited Create;
  StringsLog := Strings;
end;

procedure TStringsLogger.Log(s: string);
begin
  StringsLog.Add(s);
end;

end.
