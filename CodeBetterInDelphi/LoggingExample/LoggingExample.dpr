program LoggingExample;

uses
  Vcl.Forms,
  LoggerForm in 'LoggerForm.pas' {BusinessForm},
  StringsLogger in 'StringsLogger.pas',
  BusinessObject in 'BusinessObject.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TBusinessForm, BusinessForm);
  Application.Run;
end.
