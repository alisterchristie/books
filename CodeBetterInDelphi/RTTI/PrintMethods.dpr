program PrintMethods;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils, System.RTTI, System.IOUtils, System.TypInfo;

type
  IApplicationSettings = interface(IInvokable)
  ['{0CE04A2D-D203-4703-8D55-1A331EA77B0D}']
    function GetDefaultFolder: string;
    procedure SetDefaultFolder(const Value: string);
    property DefaultFolder: string read GetDefaultFolder write SetDefaultFolder;
  end;

procedure AProcedure(Method: TRttiMethod; const Args: TArray<TValue>;
  out Result: TValue);
var
  I: Integer;
  Parameters : TArray<TRttiParameter>;
begin
  writeln(Method.ToString);
  Parameters := Method.GetParameters;
  for I := 0 to Length(Parameters)-1 do
    writeln(' ', Method.GetParameters[i].Name, ':',
                 Args[i+1].TypeInfo.Name, '=',
                 Args[i+1].ToString);
  if Method.MethodKind = mkFunction then
    writeln(' Returns ', Method.ReturnType.ToString);
end;

var
  AppSettings : IApplicationSettings;

begin
  AppSettings := TVirtualInterface.Create
    (TypeInfo(IApplicationSettings), AProcedure) as IApplicationSettings;

  AppSettings.GetDefaultFolder;
  AppSettings.SetDefaultFolder(TPath.GetDocumentsPath);
  ReadLn;
end.

