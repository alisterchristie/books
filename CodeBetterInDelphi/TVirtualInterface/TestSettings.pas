unit TestSettings;

interface

uses
  SettingsAttributes;

type
  ITestSettings = interface(IInvokable)
  ['{AAB97083-51C1-4D08-B583-43C669A7EB20}']
    function GetTestString: String;
    procedure SetTestString(const Value: String);
    function GetTestInteger: Integer;
    procedure SetTestInteger(const Value: Integer);
    function GetTestFloat: Double;
    procedure SetTestFloat(const Value: Double);
    function GetTestBoolean: Boolean;
    procedure SetTestBoolean(const Value: Boolean);

    property TestString: String read GetTestString write SetTestString;
    property TestInteger: Integer read GetTestInteger write SetTestInteger;
    property TestFloat: Double read GetTestFloat write SetTestFloat;
    property TestBoolean: Boolean read GetTestBoolean write SetTestBoolean;

    [section('Invoicing')]
    function GetInvoiceMonth: string;
    procedure SetInvoiceMonth(const Value: string);
    property InvoiceMonth: string read GetInvoiceMonth write SetInvoiceMonth;
  end;

implementation


end.
