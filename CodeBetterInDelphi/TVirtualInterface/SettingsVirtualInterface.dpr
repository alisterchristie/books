program SettingsVirtualInterface;

uses
  Vcl.Forms,
  SettingsForm in 'SettingsForm.pas' {Form11},
  VirtualIniSettings in 'VirtualIniSettings.pas',
  TestSettings in 'TestSettings.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm11, Form11);
  Application.Run;
end.
