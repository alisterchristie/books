unit SettingsForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.WinXCtrls,
  Vcl.Samples.Spin, System.IniFiles, TestSettings;

type
  TForm11 = class(TForm)
    edtString: TEdit;
    seInteger: TSpinEdit;
    edtFloat: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    tsBoolean: TToggleSwitch;
    btnRead: TButton;
    btnWrite: TButton;
    btnMayInvoicing: TButton;
    procedure FormCreate(Sender: TObject);
    procedure btnReadClick(Sender: TObject);
    procedure btnWriteClick(Sender: TObject);
    procedure btnMayInvoicingClick(Sender: TObject);
  private
    Settings : ITestSettings;
    INI : TIniFile;
  public
    { Public declarations }
  end;

var
  Form11: TForm11;

implementation

uses
  System.IOUtils, VirtualIniSettings;

{$R *.dfm}

procedure TForm11.btnMayInvoicingClick(Sender: TObject);
begin
  Settings.InvoiceMonth := 'May';
end;

procedure TForm11.btnReadClick(Sender: TObject);
begin
  if Settings.TestBoolean then
    tsBoolean.State := tssOn
  else
    tsBoolean.State := tssOff;
  edtString.Text := Settings.TestString;
  edtFloat.Text := Settings.TestFloat.ToString;
  seInteger.Value := Settings.TestInteger;
end;

procedure TForm11.btnWriteClick(Sender: TObject);
begin
  Settings.TestBoolean := tsBoolean.State = tssOn;
  Settings.TestString := edtString.Text;
  Settings.TestFloat := StrToFloatDef(edtFloat.Text, 0);
  Settings.TestInteger := seInteger.Value;
end;

procedure TForm11.FormCreate(Sender: TObject);
begin
  INI := TIniFile.Create(TPath.Combine(TPath.GetDocumentsPath, 'TestSettings.ini'));
  Settings := TSettingsVirtualInterface.Create(TypeInfo(ITestSettings), INI) as ITestSettings;
end;

end.
