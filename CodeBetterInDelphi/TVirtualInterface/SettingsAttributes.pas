unit SettingsAttributes;

interface

type
  SectionAttribute = class(TCustomAttribute)
  private
    FSection : string;
  public
    constructor Create(const Section: string);
    function Section : string;
  end;

implementation

constructor SectionAttribute.Create(const Section: string);
begin
  inherited Create;
  FSection := Section;
end;

function SectionAttribute.Section: string;
begin
  result := FSection;
end;

end.
