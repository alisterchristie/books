object Form11: TForm11
  Left = 0
  Top = 0
  Caption = 'Form11'
  ClientHeight = 199
  ClientWidth = 197
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 28
    Height = 13
    Caption = 'String'
  end
  object Label2: TLabel
    Left = 16
    Top = 43
    Width = 38
    Height = 13
    Caption = 'Boolean'
  end
  object Label3: TLabel
    Left = 16
    Top = 66
    Width = 36
    Height = 13
    Caption = 'Integer'
  end
  object Label4: TLabel
    Left = 16
    Top = 94
    Width = 24
    Height = 13
    Caption = 'Float'
  end
  object edtString: TEdit
    Left = 64
    Top = 13
    Width = 121
    Height = 21
    TabOrder = 0
  end
  object seInteger: TSpinEdit
    Left = 64
    Top = 63
    Width = 121
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 1
    Value = 0
  end
  object edtFloat: TEdit
    Left = 64
    Top = 91
    Width = 121
    Height = 21
    TabOrder = 2
    Text = '0.0'
  end
  object tsBoolean: TToggleSwitch
    Left = 64
    Top = 40
    Width = 72
    Height = 20
    TabOrder = 3
  end
  object btnRead: TButton
    Left = 16
    Top = 128
    Width = 75
    Height = 25
    Caption = 'Read'
    TabOrder = 4
    OnClick = btnReadClick
  end
  object btnWrite: TButton
    Left = 110
    Top = 128
    Width = 75
    Height = 25
    Caption = 'Write'
    TabOrder = 5
    OnClick = btnWriteClick
  end
  object btnMayInvoicing: TButton
    Left = 56
    Top = 159
    Width = 89
    Height = 25
    Caption = 'May Invoicing'
    TabOrder = 6
    OnClick = btnMayInvoicingClick
  end
end
