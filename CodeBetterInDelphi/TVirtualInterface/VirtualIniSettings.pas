unit VirtualIniSettings;

interface

uses System.Rtti, System.TypInfo, System.IniFiles;

type
  TSettingsVirtualInterface = class(TVirtualInterface)
  protected
    INI : TCustomIniFile;
    PIID : PTypeInfo;
    procedure HandleMethods(Method: TRttiMethod; const Args: TArray<TValue>; out Result: TValue);
  public
    constructor Create(PIID: PTypeInfo; INI : TCustomIniFile);
  end;

implementation

uses
  SettingsAttributes;

constructor TSettingsVirtualInterface.Create(PIID: PTypeInfo; INI : TCustomIniFile);
begin
  inherited Create(PIID, HandleMethods);
  Self.Ini := INI;
  Self.PIID := PIID;
end;

procedure TSettingsVirtualInterface.HandleMethods(Method: TRttiMethod; const Args: TArray<TValue>; out Result: TValue);
var
  PropertyName : string;
  Section : string;
  Context : TRTTIContext;
  Attributes: TArray<TCustomAttribute>;
  Attribute : TCustomAttribute;
begin
  PropertyName := Copy(Method.Name, 4); { remove the first 3 characters (get/set) }
  Section := 'Settings';

  Context := TRTTIContext.Create;
  Attributes := Context.GetType(PIID).GetMethod('Get' + PropertyName).GetAttributes;
  for Attribute in Attributes do
  begin
    if Attribute is SectionAttribute then
      Section := (Attribute as SectionAttribute).Section;
  end;

  if Method.MethodKind = mkFunction then
  begin { Getter }
    case Method.ReturnType.TypeKind of
      tkInteger: Result := TValue.From<integer>(INI.ReadInteger(Section, PropertyName, 0));
      tkFloat: Result := TValue.From<double>(INI.ReadFloat(Section, PropertyName, 0));
      tkUString: Result := TValue.From<string>(INI.ReadString(Section, PropertyName, ''));
      tkEnumeration: Result := TValue.From<Boolean>(INI.ReadBool(Section, PropertyName, False));
    end;
  end
  else
  begin { Setter }
    case Args[1].Kind of
      tkInteger: INI.WriteInteger(Section, PropertyName, Args[1].AsInteger);
      tkFloat: INI.WriteFloat(Section, PropertyName, Args[1].AsExtended);
      tkUString: INI.WriteString(Section, PropertyName, Args[1].AsString);
      tkEnumeration: INI.WriteBool(Section, PropertyName, Args[1].AsBoolean);
    end;
  end;
end;

end.
